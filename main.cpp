#include <iostream>
#include <exception>

#include "level.hpp"

using namespace std;

int main(int argc, char *argv[])
{
  if( argc < 2 ){
    cout<< "usage: "<<argv[0]<<" levelFileName"<<endl;
    return -1;
  }

  try{
    Level lvl(argv[1]);

    cout<<"Done."<<endl;

    cout<<lvl.currentRoomDescription()<<endl;
    string results;
    string input;
    while(results != "null"&&lvl.isRunning()){
      cin>>input;
      results = lvl.handleInput(input);
      if(results != "null") cout<<results<<endl;
    }
      cout<<"Thank you for playing!"<<endl;
  }catch(int code){
    cout<<"EXCEPTION code:"<<code<<endl;
    return -1;
  }catch(string msg){
    cout<<"EXCEPTION message:"<<msg<<endl;
    return -1;
  }catch(exception& e){
    cout<<"EXCEPTION:"<<e.what()<<endl;
    return -1;
  }catch(...){
    cout<<"EXCEPTION"<<endl;
    return -1;
  }

  return 0;
}
