LIBS= -framework OpenAL -lvorbis -lvorbisenc -lvorbisfile
INC=-I./jsmn/ -I./vorbis/include/ -I./libogg-1.3.1/include
JSMNSRC=jsmn/jsmn.c
OGGSRC=libogg-1.3.1/src/bitwise.c libogg-1.3.1/src/framing.c
ALUTSRC=freealut/src/alutBufferData.c freealut/src/alutCodec.c freealut/src/alutError.c freealut/src/alutInit.c freealut/src/alutInputStream.c freealut/src/alutOutputStream.c freealut/src/alutUtil.c freealut/src/alutVersion.c freealut/src/alutWaveform.c
SRC=main.cpp $(JSMNSRC) $(OGGSRC) 
CFLAG=-std=c++11 
DEBUG=-v -g
CC=clang++
PROG=ta

all:$(PROG)

$(PROG):
	$(CC) $(CFLAG) $(INC) $(LIBS) -o $(PROG) $(SRC)

debug:
	$(CC) $(CFLAG) $(INC) $(LIBS) -o $(PROG) $(DEBUG) $(SRC)

clean:
	rm ta

.PHONY: all clean
