#ifndef _OGGFILE_H_
#define _OGGFILE_H_

#include "soundfile.hpp"
#include <string>
#include <iostream>

using namespace std;

#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#include <ogg/ogg.h>
#include <vorbis/codec.h>
#include <vorbis/vorbisenc.h>
#include <vorbis/vorbisfile.h>


class OggFile:public SoundFile{
public:
  OggFile():isOpen(false){};
  ~OggFile();
  void open(const string filename);
  void release();
  string getInfo();
  string getFilename();
  bool play();
  bool playing();
  bool update();
protected:
  bool stream(ALuint buffer);
  void empty();
bool check(int line,bool enableThrow = true);
  string errorString(int code);
private:
  string          filename;
  bool            isOpen;
  FILE*           file;
  OggVorbis_File  oggStream;
  vorbis_info*    vorbisInfo;
  vorbis_comment* vorbisComment;

  ALuint          buffers[2];
  ALuint          source;
  ALenum          format;
};

#include "oggfile.impl"

#endif /* _OGGFILE_H_ */
