#ifndef _SOUNDFILE_H_
#define _SOUNDFILE_H_

class SoundFile{
public:
  virtual void open(std::string filename) = 0;
  virtual void release() = 0;
  virtual std::string getInfo() = 0;
  virtual std::string getFilename() = 0;
  virtual bool play() = 0;
  virtual bool playing() = 0;
  virtual bool update() = 0;
};

#endif /* _SOUNDFILE_H_ */
