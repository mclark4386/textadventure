#ifndef _SOUNDMANAGER_H_
#define _SOUNDMANAGER_H_

#include <iostream>
#include <map>
#include <string>
#include <future>
#include <vector>

#include "oggfile.hpp"
#include "soundfile.hpp"

#include <mutex>
#include <atomic>

using namespace std;

class SoundManager
{
  map<string,OggFile> sources;
  future<void> streamThread;
  ALCdevice* device;
  ALCcontext* context;
  bool running;
  mutex sourcesMutex;
  string fileToOpen;
  mutex fileMutex;
public:
  SoundManager(){cout<<"loading sound"<<endl;fileToOpen = ""; running = true; streamThread = std::async(std::launch::async,[&](){this->initSoundManager();});};
  ~SoundManager(){running = false; streamThread.get();alcMakeContextCurrent(NULL);alcDestroyContext(context);alcCloseDevice(device);};
  
  void initSoundManager();
  void addSource(string filename, bool looping = false);
  void removeSource(string filename);

  string getSourceInfo(string filesname);

private:
  void addFile(string filename, bool looping = false);
};

void SoundManager::initSoundManager(){
  device = alcOpenDevice(NULL);
  if(!device){
    throw string("Couldn't get device.");
  }

  context = alcCreateContext(device,NULL);
  alcMakeContextCurrent(context);
  if(!context){
    throw string("Couldn't get context.");
  }

  ALfloat pos[] = {0.0,0.0,4.0};
  ALfloat vel[] = {0.0,0.0,0.0};
  ALfloat ori[] = {0.0,0.0,1.0,0.0,1.0,0.0};
  alListenerfv(AL_POSITION,pos);
  alListenerfv(AL_VELOCITY,vel);
  alListenerfv(AL_ORIENTATION,ori);

  cout<<"OpenAL inited"<<endl;
  
  while(running){
    fileMutex.lock();
    if(fileToOpen != ""){
      if(sources.find(fileToOpen) == sources.end()){
	string filename;
	fileToOpen = "";
	filename = fileToOpen;
	fileMutex.unlock();

	sources[filename].play();
      }else{
	string filename;
	filename = fileToOpen;
	fileToOpen = "";
	fileMutex.unlock();

	addFile(filename);
      }
    }
    fileMutex.unlock();

    for(auto& key:sources){
      try{
	lock_guard<mutex> lock(sourcesMutex);
	while(key.second.update()){
	  if(!key.second.playing()){
	    if(!key.second.play()){
#ifndef DONT_THROW_ON_AL
	      throw string("Ogg abruptly stopped.");
#endif
	    }else{
	      cout<<"Ogg stream was interrupted."<<endl;
	    }
	  }
	} 
      }catch(string e){
	sources.erase(key.first);
	cout<<"OpenAL EXCEPTION:"<<e<<endl;
      }      
    }
  }
}
void SoundManager::addSource(string filename,bool looping){
  lock_guard<mutex> lock(fileMutex);
  fileToOpen = filename;
}

void SoundManager::addFile(string filename,bool looping){
  alGetError();//clear error queue
  //load data
  
  OggFile ogg;
  try{
    ogg.open(filename);
  }catch(string s){
    cout<<"got open exception;"<<s<<endl;
    return;
  }

  lock_guard<mutex> lock(sourcesMutex);
  sources[filename] = ogg;
  
  //debugging
  //  cout<<sources[filename].getInfo()<<endl;
  
  if (!sources[filename].play())
    throw string("Ogg refused to play.");
  
  //  streams.push_back(std::async(std::launch::async,[&](){
  //	alcMakeContextCurrent(context);
  //	OggFile& sound = sources[filename];
  //	while(sound.update()){
  //	  if(!sound.playing()){
  //	    if(!sound.play()){
  //#ifndef DONT_THROW_ON_AL
  //	      throw string("Ogg abruptly stopped.");
  //#endif
  //	    }else{
  //	      cout<<"Ogg stream was interrupted."<<endl;
  //	    }
  //	  }
  //	}
  //      }));
}

#endif /* _SOUNDMANAGER_H_ */
