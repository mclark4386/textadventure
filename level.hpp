#ifndef _LEVEL_H_
#define _LEVEL_H_

#include <jsmn.h>
#include <string>
#include <cerrno>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <utility>
#include <iterator>
#include <iostream>
#include <sstream>

#include "soundmanager.hpp"

using namespace std;

string getFileContents(const char* filename);
string jsonTypeToString(jsmntype_t type);
bool is_number(const string str);

class Level{
  jsmn_parser root;
  jsmntok_t tokens[256];
  string level;
  size_t currentRoom;

  SoundManager sound;
public:
  string name;
  vector< map<string,string> > rooms;
  
  Level(const char* filename){loadLevel(filename);};
  
  void loadLevel(const char* filename);

  string currentRoomDescription();
  string handleInput(string input);
  bool isRunning();

private:
  string parseCommands(const string cmds);
};

void Level::loadLevel(const char* filename){
  level = getFileContents(filename);
  currentRoom = 0;
  
  //cout<<"level: \n"<<level<<endl;
  
  jsmn_init(&root);
  int r = jsmn_parse(&root,level.c_str(),tokens,256);
  if(r == JSMN_SUCCESS){
    if(tokens[0].type != JSMN_OBJECT){
      cerr<<"Level file is not in the right format"<<endl;
      throw(-1);
    }
    //cout<<"root: beg:"<<tokens[0].start<<" end:"<<tokens[0].end<<" size:"<<tokens[0].size<<endl;
    
    map<string,size_t> json;
    for(int i=1;i<tokens[0].size;i++){
      //cout<<"token:"<<i<<" beg:"<<tokens[i].start<<" end:"<<tokens[i].end<<" size:"<<tokens[i].size<<endl;
      //cout<<" value:"<<1+i<<" beg:"<<tokens[i+1].start<<" end:"<<tokens[i+1].end<<" size:"<<tokens[i+1].size<<endl;
      string key = level.substr(tokens[i].start,tokens[i].end-tokens[i].start);
      json[key] = ++i;
      //      cout<<"key:"<<key<<" value:"<<json[key]<<endl;
    }
    
    for(auto it = json.begin();it != json.end();it++){
      jsmntok_t* jsonObject = &tokens[it->second];
      if(it->first == "name"&&jsonObject->type == JSMN_STRING){
	name = level.substr(jsonObject->start,(jsonObject->end-jsonObject->start));
	cout<<"Loading level "<<name<<"..."<<endl;
      }
      else if(it->first == "rooms"&&jsonObject->type == JSMN_ARRAY){
	//cout<<"rooms: beg:"<<jsonObject->start<<" end:"<<jsonObject->end<<" size:"<<jsonObject->size<<endl;
	int offset = (int)it->second;
	//	for(int i=0;i<jsonObject->size;i++){
	  int localOffset = offset;
	  jsmntok_t* rawRoom = &tokens[localOffset];
	  //cout<<"i:"<<i<<" size:"<<jsonObject->size<<endl;
	  //cout<<"   rawRoom:"<<level.substr(rawRoom->start,rawRoom->end-rawRoom->start)<<" type:"<<jsonTypeToString(rawRoom->type)<<endl;
	  //	    localOffset++;
	  //we need to add the num of subtokens to offset at some point
	  for(int j=0;j<rawRoom->size&&tokens[localOffset+1].start != -1&&tokens[localOffset+1].end != -1&&tokens[localOffset+1].type <= JSMN_STRING;j++){
	    localOffset++;
	    //cout<<"     room:"<<" type:("<<tokens[localOffset].type<<")"<<jsonTypeToString(tokens[localOffset].type)<<" size:"<<tokens[localOffset].size<<endl<<"name:"<<level.substr(tokens[localOffset].start,tokens[localOffset].end-tokens[localOffset].start)<<endl;
	    map<string,string> json;
	    int roomOffset = localOffset;
	    for(int g=0;g<tokens[roomOffset].size;g++){
	      localOffset++;
	      string key = level.substr(tokens[localOffset].start,tokens[localOffset].end-tokens[localOffset].start);
	      localOffset++;
	      g++;
	      string value = level.substr(tokens[localOffset].start,tokens[localOffset].end-tokens[localOffset].start);
	      json[key] = value;
	      //cout<<"     adding key:"<<key<<" with value:"<<json[key]<<endl;
	    }
	    rooms.push_back(json);
	  }
	
	  //offset = ++localOffset;
	  //}
      }
      else{
	cout<<"not room or name,key:"<<it->first<<" type is "<<jsonTypeToString(tokens[it->second].type)<<endl;
      }
    }
    
    //cout<<"Done."<<endl;
  }else{
    cerr<<"Couldn't parse the level file"<<endl;
    throw(-1);
  }
}

string Level::currentRoomDescription(){
  if(currentRoom >= rooms.size()){
    cout<<"room out of bounds"<<endl;
    return "null";
  }

  //  cout<<"getting room at index:"<<currentRoom<<endl;
  map<string,string> room(rooms[currentRoom]);
  
  //  for(auto& k:room){
  // cout<<"room item:|"<<k.first<<"|:"<<k.second<<endl;
  //  }

  if(room.find("play") != room.end() ){
    //play sound room["play"]
    cout<<"Found a play attribute:"<<room["play"]<<endl;
    sound.addSource(room["play"],true);
  }
  
  if(room.find("desc") != room.end() ){
    //cout<<"has desc:"<<room["desc"];
    return room["desc"];
  }

  //cout<<"doesn't have desc"<<endl;
  return "A mysterious void.";
}

string Level::handleInput(string input){
  if(currentRoom >= rooms.size()){
    //cout<<"room out of bounds"<<endl;
    return "null";
  }

  //cout<<"getting room at index:"<<currentRoom<<endl;
  map<string,string> room(rooms[currentRoom]);
  
  for(auto& k:room){
    //   cout<<"room item:"<<k.first<<":"<<k.second<<endl;
    if(k.first == "next" && k.second == "null"){
      return "null";
    }
  }
  
  if(room.find(input) != room.end() ){
    //    cout<<"VALID INPUT!:"<<input<<" has value:"<<room[input]<<endl;
    if(is_number(room[input])){
      stringstream ss;
      size_t index;
      ss<<room[input];
      ss>>index;
      currentRoom = index;
      return currentRoomDescription();
    }else{
      return parseCommands(room[input]);
    }
  }
  return "I don't know what you mean.";
}

bool Level::isRunning(){
  if(currentRoom >= rooms.size()){
    //cout<<"room out of bounds"<<endl;
    return false;
  }

  //cout<<"getting room at index:"<<currentRoom<<endl;
  map<string,string> room(rooms[currentRoom]);
  
  for(auto& k:room){
    //   cout<<"room item:"<<k.first<<":"<<k.second<<endl;
    if(k.first == "next" && k.second == "null"){
      return false;
    }
  }
  return true;
}

string Level::parseCommands(const string cmds){
  //put your scripting here
  return cmds;
}

bool is_number(const string str){
  return !str.empty()&&find_if(str.begin(), str.end(), [](char c){return !isdigit(c);}) == str.end();
}
  
string getFileContents(const char* filename){
  ifstream in(filename, ios::in|ios::binary);
  if(in){
    string contents;
    in.seekg(0,ios::end);
    contents.resize(in.tellg());
    in.seekg(0,ios::beg);
    in.read(&contents[0],contents.size());
    in.close();
    return contents;
  }
  cerr<<"ERROR reading contents of file:"<<filename<<endl;
  throw(errno);
}

string jsonTypeToString(jsmntype_t type){
  if(type == JSMN_OBJECT){
    return "OBJECT";
  }
  if(type == JSMN_PRIMITIVE){
    return "PRIMITIVE";
  }
  if(type == JSMN_ARRAY){
    return "ARRAY";
  }
  if(type == JSMN_STRING){
    return "STRING";
  }
  return "UNKNOWN";
}

#endif /* _LEVEL_H_ */
